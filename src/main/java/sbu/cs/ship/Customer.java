package sbu.cs.ship;

import java.util.List;

public class Customer {

    private String name;
    private String id;

    public Customer(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void reserve(Color color, Company company, int days) {
        System.out.println(this.name + " want to reserve a " + color + " ship");
        List<Ship> ships = company.shipAvailable(color);
        if (ships.isEmpty()) {
            System.out.println(color + " ship does not exist");
            return;
        }
        Ship minPriceShip = ships.get(0);
        for (Ship ship : ships) {
            if (ship.getPrice() < minPriceShip.getPrice()) {
                minPriceShip = ship;
            }
        }
        System.out.println(this.name + " reserved " + minPriceShip.toString());
        company.reserveShip(minPriceShip, this, days);
    }
}
