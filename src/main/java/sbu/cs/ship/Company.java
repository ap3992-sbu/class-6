package sbu.cs.ship;

import java.util.*;

public class Company {

    private String name;
    private List<Contract> contracts = new ArrayList<>();
    private Set<Ship> ships = new HashSet<>();

    public Company(String name) {
        this.name = name;
    }

    public Company() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Contract> getContracts() {
        return contracts;
    }

    public void addContract(Contract contract) {
        this.contracts.add(contract);
    }

    public Set<Ship> getShips() {
        return ships;
    }

    public void addShip(Ship ship) {
        this.ships.add(ship);
    }

    /*
    *
     */
    public List<Ship> shipAvailable(Color color) {
        this.reloadShipAvailability();
        List<Ship> shipsAvailable = new ArrayList<>();
        for (Ship ship : this.ships) {
            if (ship.getColor().equals(color) && ship.isAvailable()) {
                shipsAvailable.add(ship);
            }
        }
        return shipsAvailable;
    }

    public void reserveShip(Ship ship, Customer customer, int days) {
        if (! ship.isAvailable()) {
            throw new RuntimeException();
        }
        ship.setAvailable(false);
        Contract contract = new Contract(
                customer
                , ship.getPrice() * days
                , new Date()
                , ship
                , days
        );
        this.addContract(contract);
    }

    public void reloadShipAvailability() {
        for (Contract contract : this.contracts) {
            contract.checkEnd();
        }
    }
}
