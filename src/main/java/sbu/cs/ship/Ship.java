package sbu.cs.ship;

public class Ship {

    private String sid;
    private Color color;
    private double price;
    private boolean isAvailable;

    public Ship(String sid, Color color, double price) {
        this.sid = sid;
        this.color = color;
        this.price = price;
        this.isAvailable = true;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "sid='" + sid + '\'' +
                ", color=" + color +
                ", price=" + price +
                ", isAvailable=" + isAvailable +
                '}';
    }
}
