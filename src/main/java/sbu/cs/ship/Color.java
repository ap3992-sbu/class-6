package sbu.cs.ship;

public enum Color {
    BLUE,
    GREEN,
    RED,
    GREY,
    PINK
}
